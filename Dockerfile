ARG BASE_IMG=registry.gitlab.com/beerlab/cpc/utils/cps_ros_base_docker:latest

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-ci"]

# Timezone Configuration
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y \
    ros-noetic-octomap ros-noetic-octomap-msgs ros-noetic-octomap-ros && \
    rm -rf /var/lib/apt/lists/*
RUN apt update
RUN apt install ros-noetic-octomap-server
WORKDIR /ros_ws

COPY . /ros_ws/src/octomap_mapping
RUN catkin build && source devel/setup.bash

CMD ["/bin/bash", "-ci", "roslaunch octomap_server octomap_tracking_server.launch path:=/ros_ws/sprint_501.bt"]
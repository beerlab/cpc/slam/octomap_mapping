# Как и когда пользоваться с ORB-SLAM3
Этот модуль может использоваться просто для построения октомапы, но далее пойдёт речь, как его использовать с ORB-SLAM3. Весь алгоритм состоит из двух этапов, но можно пропустить первый этап в случае если уже есть нужные файлы:

1. Прекартирование : картирование в ORB-SLAM3 и сохраняем карту -> на выходе должна быть карта в формате .osa и росбаг
2. Картирование : картирование в octomap_server с использованием ORB_SLAM3 -> на выходе должна быть карта в формате .bt

## 1. Прекартирование
В первую очередь запишите карту в ORB-SLAM3 командой:
```
rosrun ORB_SLAM3 Stereo_Inertial Vocabulary/ORBvoc.txt Examples/Stereo-Inertial/RealSense_D435i_save.yaml false 0
```
###### *пример для запуска топиков с RealSeanse
```
roslaunch realsense2_camera rs_camera.launch enable_depth:=true depth_width:=640 depth_height:=480 depth_fps:=30 enable_infra1:=true infra_width:=640 infra_height:=480 enable_infra2:=true enable_gyro:=true enable_accel:=true unite_imu_method:=linear_interpolation filters:=pointcloud
```
А так же запишите росбаг с топиками камеры, tf, глубины, clock(опционально)

###### *пример для записи с RealSeanse
```
rosbag record -O last.bag /camera/imu /camera/infra1/image_rect_raw /camera/infra2/image_rect_raw /camera/depth/color/points /tf /tf_static
```

В конце картирования нажмите галочку step by step и кнопку STOP для сохранения карты ORB-SLAM3

## 2. Картирование
#### Для этого этапа нужен файлы .osa и .bag
Запускаем .bag файл с праметром -r 0.2 (скорость можно изменять)
###### *пример
```
rosbag play -r 0.2 last.bag 
```

Запускаем ORB-SLAM3 в режиме локализации (желательно через секунд 6-10 после начала росбага) с использованием записанной карты на предыдущем этапе
```
rosrun ORB_SLAM3 Stereo_Inertial Vocabulary/ORBvoc.txt Examples/Stereo-Inertial/RealSense_D435i_load.yaml false 1
```
Теперь запускаем octomap для картирования:
```
roslaunch octomap_server octomap_mapping.launch
```
После достаточно построенной карты нужно прервать работу rasbaga. И сохранить карту командой(ВАЖНО не закрывая процесс выполнения ocromap_server):
```
rosrun octomap_server octomap_saver -f last.bt
```
## Как смотреть что происходит?
В RVIZ поставить fixed_frame parent_frame, а карту, которую строит октомап, можно смотреть через occupancy grid


octomap_mapping ![CI](https://github.com/OctoMap/octomap_mapping/workflows/CI/badge.svg)
===============

ROS stack for mapping with OctoMap, contains the `octomap_server` package.

The main branch for ROS1 Kinetic, Melodic, and Noetic is `kinetic-devel`.

The main branch for ROS2 Foxy and newer is `ros2`.
